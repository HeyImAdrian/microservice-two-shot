from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hat, LocationVO
from common.json import ModelEncoder



class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href", "section_number", "shelf_number"]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url"
        ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "location": o.location.closet_name # this changes the value returned in json, check insomnia 
        }


    # def get_extra_data(self, o):
    #     count = LocationVO.objects.filter(email=o.email).count() # don't understand this part
    #     return {"has_account": count > 0}


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):

    if request.method == "GET":
       #hats = Hat.objects.filter(location=location_vo_id)
        if location_vo_id is not None: # if they request a specific location // this filters out detail view requests
            hats = Hat.objects.filter(location=location_vo_id) # filter hat objects through the location vo id which communicates
            # which gets the id from the communication model
        else:
            hats = Hat.objects.all() # if a simple id-less get request is sent, include all Hat objects
        return JsonResponse(   # return response in this format : hats with the content values in the hat list encoder
                {"hats": hats},
                encoder=HatDetailEncoder,
            )

    else:    #### code for Post method

        content = json.loads(request.body)   # name a variable that holds json response

        # Get the Hat object and put it in the content dict
    try:
        location_href = content["location"] #
        location = LocationVO.objects.get(import_href=location_href) # get
        content["location"] = location
    except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    hat = Hat.objects.create(**content)
    return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_hat(request, pk):
     if request.method == "DELETE":
            count, _ = Hat.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})






# Post



# Delete
