# Wardrobify

Team:

* Person 1 - Which microservice?
Adrian Olivares - Shoes Microservice
* Person 2 - Which microservice?
Alana T. - Hats Microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The way I'll implement the integration between the Shoes microservice and wardrobe microservice is by first creating a shoes/api django app. Here I will convert the API into CRUD where I will be able to create, read, and delete different things within my model and add that to my database. The polling application would then be updated to be able to pull Bin data from the Wardrop API.

## Hats microservice

The Hats microservice will need to contain at least two models:

1. LocationVO, which will poll the port on Wardrobe to get the Location model href
2. Hat, which will talk to the LocationVO to get the specific import_href that LocationVO has access to from the Location model in Wardrobe
