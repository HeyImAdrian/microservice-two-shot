from django.db import models

# Create your models here.


class BinVO(models.Model):
    closet_name = models.CharField(max_length=250)
    bin_number = models.PositiveBigIntegerField()
    bin_size = models.PositiveBigIntegerField()
    import_href = models.CharField(unique=True, max_length=250)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=250)
    model_name = models.CharField(max_length=250)
    color = models.CharField(max_length=250)
    picture_url = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE
    )
