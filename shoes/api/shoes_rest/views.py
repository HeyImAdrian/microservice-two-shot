from .models import BinVO, Shoe
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]




class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
    ]
    encoders = {
        "bins": BinVOEncoder()
    }
    def get_extra_data(self, o):
        return {
            "Bin": o.bin.closet_name
        }


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoe":shoe},
            encoder=ShoeEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            href = content["bin"]
            bin = BinVO.objects.get(import_href=href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status = 400
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_shoes(request,id):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
