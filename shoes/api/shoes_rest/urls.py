from django.urls import path
from .views import api_shoes, api_list_shoes

urlpatterns = [
    path("shoe/", api_list_shoes, name="api_list_shoes"),
    path("shoe/<int:id>/", api_shoes, name="api_shoe")
]
