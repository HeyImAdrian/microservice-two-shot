import { useState, useEffect } from "react";


const ShoeList = () => {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const response= await fetch("http://localhost:8080/api/shoe")
        if (response.ok) {
            const data = await response.json()
            setShoes(data.shoe)
        }
    }
    useEffect(()=> {
        getData()
    }, [])

    const handleDelete = async (e) => {
        const url = `http://localhost:8080/api/shoe/${e.target.id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()


        setShoes(shoes.filter(bin => String(bin.id) !== e.target.id))
    }



    return(
        <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Picture URL</th>
          <th>Closet Name</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={shoe.href}>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>{shoe.picture_url}</td>
              <td>{shoe.Bin}</td>
              <td><button onClick={handleDelete} id={shoe.id} className="btn btn-danger">Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
    )
}

export default ShoeList
