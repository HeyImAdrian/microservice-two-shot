


import { useEffect, useState } from 'react';

function HatsList() {
  const [hats, setHats] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/hats/');

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  const handleDelete = async (e) => {
    const url = `http://localhost:8090/api/hats/${e.target.id}`

    const fetchConfigs = {
        method: "Delete",
        headers: {
            "Content-Type": "application/json"
        }
    }

    const resp = await fetch(url, fetchConfigs)
    const data = await resp.json()

    setHats(hats.filter(location => String(location.id) !== e.target.id))
    // getData()
}



  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Fabric</th>
          <th>Pic Url</th>
          <th>Style</th>
          <th>Color</th>
          <th>Closet Name</th>




        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.id}>
              <td>{ hat.id }</td>
              <td>{ hat.fabric }</td>
              <td>{ hat.picture_url }</td>
              <td>{ hat.style_name }</td>
              <td>{ hat.color }</td>
              <td>{ hat.location }</td>
              <td><button onClick={handleDelete} id={hat.id} className="btn btn-danger">Delete</button></td>



            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;
