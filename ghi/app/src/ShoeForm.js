import React, {useState, useEffect} from 'react';

function ShoeForm() {
  const [bin, setShoes] = useState([])

  //Notice that we can condense all formData
  //into one state object
  const [formData, setFormData] = useState({

    manufacturer: '',
    model_name: '',
    color: '',
    picture_url: '',
    bin: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setShoes(data.bins);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/shoe/';

    const fetchConfig = {
      method: "post",
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    console.log(response)

    if (response.ok) {
      //The single formData object
      //also allows for easier clearing of data
      setFormData({

        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin :'',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    //We can condense our form data event handling
    //into on function by using the input name to update it

    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,


      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
  }
  console.log(formData)
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.manufacturer} placeholder="Name" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="Model Name">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.model_name} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
              <label htmlFor="Model Name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="Color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="Picture Url">Picture Url</label>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.bins} required name="bin" id="bin" className="form-select">
                <option value="">Choose a Closet</option>
                {bin.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
